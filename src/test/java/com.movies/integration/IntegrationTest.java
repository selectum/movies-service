package com.movies.integration;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.movies.config.AppConfig;
import com.movies.dto.GenresListDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class IntegrationTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testMovieFound() throws Exception {
        mockMvc.perform(get("/movies/777"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    public void testMovieNotFound() throws Exception {
        mockMvc.perform(get("/movies/999"))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    public void testXmlResponse() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/genres");
        requestBuilder.accept("application/xml");
        MvcResult mvcResult = mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/xml;charset=UTF-8"))
            .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        XmlMapper xmlMapper = new XmlMapper();
        GenresListDTO genresListDTO = xmlMapper.readValue(content, GenresListDTO.class);
        Assert.assertNotNull(genresListDTO);  // ensure we get an xml document
    }

    @Test
    public void testJsonResponse() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/genres");
        requestBuilder.accept("application/json");
        MvcResult mvcResult = mockMvc.perform(requestBuilder)
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"))
            .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        ObjectMapper jsonMapper = new ObjectMapper();
        JsonNode rootNode = jsonMapper.readTree(content);
        GenresListDTO genresListDTO = jsonMapper.readValue(rootNode, GenresListDTO.class);
        Assert.assertNotNull(genresListDTO); // ensure we have a json object
    }
}