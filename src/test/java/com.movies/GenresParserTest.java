package com.movies;

import com.mashape.unirest.http.HttpResponse;
import com.movies.config.AppConfig;
import com.movies.dto.GenreDTO;
import com.movies.dto.GenresListDTO;
import com.movies.services.ResponseParsingService;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class GenresParserTest {

    private ObjectMapper jsonMapper = new ObjectMapper();

    @Autowired
    private ResponseParsingService responseParsingService;

    @Test
    @SuppressWarnings("unchecked")
    public void testGenresListParse() throws IOException {
        List<GenreDTO> genreDTOS = Arrays.asList(
            new GenreDTO(1, "Comedy"),
            new GenreDTO(2, "Horror"),
            new GenreDTO(3, "Other")
        );
        GenresListDTO listDTO = new GenresListDTO(genreDTOS);

        String jsonBody = jsonMapper.writeValueAsString(listDTO);
        HttpResponse response = Mockito.mock(HttpResponse.class);
        Mockito.when(response.getBody()).thenReturn(jsonBody);

        GenresListDTO parsedListDTO = responseParsingService.parseGenres(response);
        Assert.assertEquals(listDTO, parsedListDTO);
    }

}