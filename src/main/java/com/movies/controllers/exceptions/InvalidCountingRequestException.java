package com.movies.controllers.exceptions;

import lombok.Getter;

@Getter
public class InvalidCountingRequestException extends RuntimeException {

    private Integer invalidGenreId;

    public InvalidCountingRequestException(Integer invalidGenreId) {
        this.invalidGenreId = invalidGenreId;
    }
}
