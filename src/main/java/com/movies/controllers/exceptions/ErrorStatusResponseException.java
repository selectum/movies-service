package com.movies.controllers.exceptions;

public class ErrorStatusResponseException extends RuntimeException {

    public ErrorStatusResponseException(String message) {
        super(message);
    }
}
