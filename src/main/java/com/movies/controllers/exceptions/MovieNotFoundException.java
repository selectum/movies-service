package com.movies.controllers.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MovieNotFoundException extends RuntimeException {

    private int id;
}
