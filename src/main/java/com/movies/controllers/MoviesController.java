package com.movies.controllers;

import com.movies.dto.MovieDTO;
import com.movies.dto.MoviesPageDTO;
import com.movies.services.MoviesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@EnableWebMvc
@RestController
@RequestMapping("/movies")
public class MoviesController extends BaseController {

    @Autowired
    private MoviesService moviesService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public MovieDTO getMovie(@PathVariable Integer id) {
        return moviesService.getMovie(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public MoviesPageDTO getMoviesPage(
            @RequestParam(value = "page", required = false) Integer pageNumber,
            @RequestParam(value = "sort_by", required = false) String sortBy,
            @RequestParam(value = "genres", required = false) List<String> genreIds
    ) {
        return moviesService.getMoviesPage(pageNumber, sortBy, genreIds);
    }

}
