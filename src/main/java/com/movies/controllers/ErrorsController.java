package com.movies.controllers;

import com.movies.controllers.exceptions.ErrorStatusResponseException;
import com.movies.controllers.exceptions.InvalidCountingRequestException;
import com.movies.controllers.exceptions.MovieNotFoundException;
import com.movies.controllers.exceptions.ParsingException;
import com.movies.dto.errors.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
@RestController
public class ErrorsController extends BaseController {

    @ExceptionHandler(value = MovieNotFoundException.class)
    @ResponseBody
    public ResponseEntity<MovieNotFoundDTO> movieNotFound(MovieNotFoundException ex) {
        return new ResponseEntity<>(new MovieNotFoundDTO(ex.getId()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    @ResponseBody
    public ResponseEntity<BadRequestDTO> badRequest() {
        return new ResponseEntity<>(BadRequestDTO.INSTANCE, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ErrorStatusResponseException.class)
    @ResponseBody
    public ResponseEntity<InternalErrorDTO> errorStatusResponseNotOk() {
        return new ResponseEntity<>(InternalErrorDTO.INSTANCE, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = ParsingException.class)
    @ResponseBody
    public ResponseEntity<ParsingErrorDTO> parsingError() {
        return new ResponseEntity<>(ParsingErrorDTO.INSTANCE, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = InvalidCountingRequestException.class)
    @ResponseBody
    public ResponseEntity<InvalidCountingRequestDTO> invalidCountingRequest(InvalidCountingRequestException ex) {
        return new ResponseEntity<>(new InvalidCountingRequestDTO(ex.getInvalidGenreId()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseEntity<InternalErrorDTO> handleUnexpectedException(Exception ex) {
        log.error("Unexpected exception occurred", ex);
        return new ResponseEntity<>(InternalErrorDTO.INSTANCE, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
