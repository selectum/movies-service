package com.movies.controllers;

import com.movies.dto.GenresListDTO;
import com.movies.services.MoviesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@RestController
@RequestMapping("/genres")
public class GenresController extends BaseController {

    @Autowired
    private MoviesService moviesService;

    @RequestMapping(method = RequestMethod.GET)
    public GenresListDTO getAllGenres() {
        return moviesService.getAllGenres();
    }

}
