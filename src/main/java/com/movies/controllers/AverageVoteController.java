package com.movies.controllers;

import com.movies.controllers.exceptions.InvalidCountingRequestException;
import com.movies.dto.AverageVoteDTO;
import com.movies.dto.GenreDTO;
import com.movies.services.AverageVoteService;
import com.movies.services.MoviesService;
import com.movies.services.averageVote.VotesCountingSnapshot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@EnableWebMvc
@RestController
@RequestMapping("/votes")
public class AverageVoteController extends BaseController {

    @Autowired
    private AverageVoteService voteService;
    @Autowired
    private MoviesService moviesService;

    @RequestMapping(value = "/{genreId}", method = RequestMethod.GET)
    public AverageVoteDTO getAverageVote(@PathVariable Integer genreId) {
        if (voteService.hasResultsFor(genreId)) {

            VotesCountingSnapshot countingSnapshot = voteService.getVotesCountingSnapshot(genreId);
            double averageVote = round(countingSnapshot.getAverageVote());

            return countingSnapshot.isComplete() ?
                AverageVoteDTO.getResult(genreId, averageVote) :
                AverageVoteDTO.getProgress(genreId, countingSnapshot.getProgress(), averageVote);
        }

        if (isValidGenre(genreId)) {
            voteService.startCountingVotes(genreId);
            return AverageVoteDTO.getStarted(genreId);
        } else {
            throw new InvalidCountingRequestException(genreId);
        }
    }

    private boolean isValidGenre(Integer genreId) {
        List<GenreDTO> genres = moviesService.getAllGenres().getGenres();
        return genres.stream().
            map(GenreDTO::getId)
            .anyMatch(id -> id.equals(genreId));
    }

    private static double round(double value) {
        return new BigDecimal(value).setScale(1, RoundingMode.HALF_UP).doubleValue();
    }

}
