package com.movies.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

@Configuration
@Import(value = {
    WebMvcConfig.class,
    ServiceConfig.class
})

public class AppConfig {

    @Autowired
    private Environment environment;

}
