package com.movies.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
@ComponentScan("com.movies")
public class ServiceConfig {

    @Bean
    public ExecutorService getExecutorService() {
        return new ThreadPoolExecutor(5, 45,
                60L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>()
        );
    }
}
