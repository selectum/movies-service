package com.movies.services.averageVote;

import com.movies.dto.MoviesPageDTO;
import com.movies.services.MoviesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

@Component
public class MoviesPagesRequesting {

    private static final Logger log = LoggerFactory.getLogger(MoviesPagesRequesting.class);

    // we get 40 api requests per 10 seconds so let's sleep for half a second for sure
    private static final int SLEEPING_TIME = 500;

    @Autowired
    private ExecutorService executorService;
    @Autowired
    private MoviesService moviesService;

    private volatile boolean isMakingRequests = true;

    private BlockingQueue<Callable<MoviePageVotes>> requestsQueue = new LinkedBlockingQueue<>();
    private BlockingQueue<MoviePageVotes> moviePageVotesQueue = new LinkedBlockingQueue<>();

    @PostConstruct
    public void init() {
        executorService.execute(() -> {
            while (isMakingRequests) {
                Callable<MoviePageVotes> requestTask = getNextTask();
                Future<MoviePageVotes> future = executorService.submit(requestTask);

                try {
                    Thread.sleep(SLEEPING_TIME);
                } catch (InterruptedException e) {
                    log.error("Sleeping is interrupted", e);
                }

                try {
                    MoviePageVotes moviePageVotes = future.get(3, TimeUnit.SECONDS);
                    moviePageVotesQueue.put(moviePageVotes);
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    log.error("Future task is not completed, skipping", e);
                }
            }
        });
    }

    public void put(final Integer genreId, final int pageNumber) {
        requestsQueue.add(getMoviePageVotes(genreId, pageNumber));
    }

    public BlockingQueue<MoviePageVotes> getMoviePageVotesQueue() {
        return moviePageVotesQueue;
    }

    private Callable<MoviePageVotes> getNextTask() {
        try {
            return requestsQueue.take();
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed to get request task", e);
        }
    }

    private Callable<MoviePageVotes> getMoviePageVotes(Integer genreId, int pageNumber) {
        final String message = "movies page " + pageNumber + " for genre with id " + genreId;
        return () -> {
            List<String> genreSingleList = Collections.singletonList(genreId.toString());
            try {
                MoviesPageDTO moviesPage = moviesService.getMoviesPage(pageNumber, null, genreSingleList);
                log.info("Received " + message);
                return new MoviePageVotes(genreId, moviesPage);
            } catch (Exception ex) {
                log.error("Failed to get " + message);
                return new MoviePageVotes(genreId, null);
            }
        };
    }

    public void shutdown() {
        isMakingRequests = false;
    }

}
