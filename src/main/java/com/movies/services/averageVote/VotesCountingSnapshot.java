package com.movies.services.averageVote;

import lombok.Data;
import lombok.Getter;

@Getter
@Data
public class VotesCountingSnapshot {

    private final boolean complete;
    private final boolean counting;
    private final double averageVote;
    private final int progress;

    public VotesCountingSnapshot(boolean complete, boolean counting, double averageVote, int progress) {
        this.complete = complete;
        this.counting = counting;
        this.averageVote = averageVote;
        this.progress = progress;
    }
}
