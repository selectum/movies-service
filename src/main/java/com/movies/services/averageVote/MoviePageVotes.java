package com.movies.services.averageVote;


import com.movies.dto.MoviesPageDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MoviePageVotes {

    private Integer genreId;
    private MoviesPageDTO moviesPageDTO;

}
