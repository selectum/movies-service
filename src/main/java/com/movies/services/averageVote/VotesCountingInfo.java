package com.movies.services.averageVote;

import com.movies.dto.MoviesPageDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VotesCountingInfo {

    private double votesSum = 0;
    private int pagesProcessed = 0;
    private int totalMovies = 0;
    private boolean isComplete;
    private boolean isCounting;

    public void addPage(MoviesPageDTO moviesPage) {
        if (moviesPage != null) {
            moviesPage.getResults().forEach(movieDTO -> votesSum += movieDTO.getVote_average());
            totalMovies += moviesPage.getResults().size();
        }
        pagesProcessed++;
    }

    public double getAverageVote() {
        return totalMovies != 0 ? votesSum / totalMovies : 0;
    }

}
