package com.movies.services;

import com.movies.dto.MoviesPageDTO;
import com.movies.services.averageVote.VotesCountingInfo;
import com.movies.services.averageVote.VotesCountingSnapshot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

@Service
public class AverageVoteService extends BaseService {

    private static final int MAX_PAGES = 1000;

    @Autowired
    private ExecutorService executorService;
    @Autowired
    private MoviesService moviesService;

    private final ConcurrentHashMap<Integer, VotesCountingInfo> progressMap = new ConcurrentHashMap<>();

    public void startCountingVotes(Integer genreId) {
        synchronized (getProgressInfo(genreId)) {
            VotesCountingInfo countingInfo = getProgressInfo(genreId);
            if (!countingInfo.isCounting()) {
                countingInfo.setCounting(true);
                executorService.execute(getCountingTask(genreId));
            }
        }
    }

    private Runnable getCountingTask(Integer genreId) {
        return () -> {
            int processedPages = 0;

            while (processedPages < MAX_PAGES) {
                int pageNumber = processedPages + 1;
                try {
                    MoviesPageDTO moviesPage = moviesService.getMoviesPage(pageNumber, null, null);
                    log.info("Received movies page: " + pageNumber + " for genre " + genreId);
                    synchronized (getProgressInfo(genreId)) {
                        VotesCountingInfo countingInfo = getProgressInfo(genreId);
                        countingInfo.addPage(moviesPage);
                    }
                } catch (Exception e) {
                    log.error("Failed to get movies page " + pageNumber + " for genre " + genreId);
                }
                processedPages++;
            }

            synchronized (getProgressInfo(genreId)) {
                getProgressInfo(genreId).setComplete(true);
            }
        };
    }

    public VotesCountingSnapshot getVotesCountingSnapshot(Integer genreId) {
        synchronized (getProgressInfo(genreId)) {
            VotesCountingInfo countingInfo = getProgressInfo(genreId);
            return new VotesCountingSnapshot(
                    countingInfo.isComplete(),
                    countingInfo.isCounting(),
                    countingInfo.getAverageVote(),
                    countingInfo.getPagesProcessed() * 100 / MAX_PAGES
            );
        }
    }

    public boolean hasResultsFor(Integer genreId) {
        return progressMap.containsKey(genreId);
    }

    private VotesCountingInfo getProgressInfo(Integer genreId) {
        return progressMap.computeIfAbsent(genreId, integer -> new VotesCountingInfo());
    }
}

