package com.movies.services;

import com.mashape.unirest.http.HttpResponse;
import com.movies.controllers.exceptions.ParsingException;
import com.movies.dto.GenresListDTO;
import com.movies.dto.MovieDTO;
import com.movies.dto.MoviesPageDTO;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class ResponseParsingService extends BaseService {

    private ObjectMapper jsonMapper = new ObjectMapper();

    @PostConstruct
    private void init() {
        jsonMapper.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public MovieDTO parseMovieDTO(HttpResponse<String> response) {
        try {
            return jsonMapper.readValue(response.getBody(), MovieDTO.class);
        } catch (IOException e) {
            log.error("Failed to parse movie from response " + response.getBody());
            throw new ParsingException();
        }
    }

    public MoviesPageDTO parseMoviesPageDTO(HttpResponse<String> response) {
        try {
            return jsonMapper.readValue(response.getBody(), MoviesPageDTO.class);
        } catch (IOException e) {
            log.error("Failed to parse page from response " + response.getBody(), e);
            throw new ParsingException();
        }
    }

    public GenresListDTO parseGenres(HttpResponse<String> response) {
        try {
            return jsonMapper.readValue(response.getBody(), GenresListDTO.class);
        } catch (IOException e) {
            log.error("Failed to parse list of genres from response " + response.getBody(), e);
            throw new ParsingException();
        }
    }
}
