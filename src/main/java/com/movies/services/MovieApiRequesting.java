package com.movies.services;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.movies.controllers.exceptions.ErrorStatusResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class MovieApiRequesting {

    private final Logger log = LoggerFactory.getLogger(MovieApiRequesting.class);

    private static final String BASE_URL = "https://api.themoviedb.org/3/";

    public static final String GET_MOVIE_URL = BASE_URL + "movie/";
    public static final String GET_ALL_GENRES_URL = BASE_URL + "genre/movie/list";
    public static final String SEARCH_MOVIES_URL = BASE_URL + "discover/movie/";

    public static final int OK_STATUS = 200;
    public static final int NOT_FOUND_STATUS = 404;

    private static final String API_KEY = "72b56103e43843412a992a8d64bf96e9";


    // as practice shows we can make 20 requests per 10 seconds (20 requests counts as 40 on api.themoviedb)
    private static final int MIN_WAIT_TIME = 550;   // milliseconds

    private Instant lastRequestTime = Instant.now();

    public synchronized HttpResponse<String> sendRequest(String requestUrl) {
        if (Instant.now().minusMillis(MIN_WAIT_TIME).isBefore(lastRequestTime)) {
            try {
                Thread.sleep(MIN_WAIT_TIME);
            } catch (InterruptedException e) {
                log.error("Interrupted sleep", e);
            }
        }

        String apiParam = requestUrl.contains("?") ? "&api_key=" : "?api_key=";
        HttpResponse<String> request = request(requestUrl + apiParam + API_KEY);
        lastRequestTime = Instant.now();
        return request;
    }

    private HttpResponse<String> request(String requestUrl) {
        GetRequest getRequest = Unirest.get(requestUrl);
        try {
            return getRequest.asString();
        } catch (UnirestException e) {
            log.error("Request failed " + requestUrl, e);
            throw new ErrorStatusResponseException("Request failed, see logs");
        }
    }

}
