package com.movies.services;

import com.mashape.unirest.http.HttpResponse;
import com.movies.controllers.exceptions.ErrorStatusResponseException;
import com.movies.controllers.exceptions.MovieNotFoundException;
import com.movies.dto.GenresListDTO;
import com.movies.dto.MovieDTO;
import com.movies.dto.MoviesPageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.movies.services.MovieApiRequesting.NOT_FOUND_STATUS;
import static com.movies.services.MovieApiRequesting.OK_STATUS;

@Service
public class MoviesService extends BaseService {

    @Autowired
    private MovieApiRequesting movieApiRequesting;
    @Autowired
    private ResponseParsingService parsingService;

    public MovieDTO getMovie(int id) {
        HttpResponse<String> response = movieApiRequesting.sendRequest(MovieApiRequesting.GET_MOVIE_URL + id);
        switch (response.getStatus()) {
            case OK_STATUS:
                return parsingService.parseMovieDTO(response);
            case NOT_FOUND_STATUS:
                throw new MovieNotFoundException(id);
            default:
                throw new ErrorStatusResponseException(response.getBody());
        }
    }

    public MoviesPageDTO getMoviesPage(Integer pageNumber, String sortBy, List<String> genreIds) {
        String requestUrl = buildMoviePageUrl(pageNumber, sortBy, genreIds);
        HttpResponse<String> response = movieApiRequesting.sendRequest(requestUrl);
        if (response.getStatus() == OK_STATUS) {
            return parsingService.parseMoviesPageDTO(response);
        } else {
            throw new ErrorStatusResponseException(response.getBody());
        }
    }

    public GenresListDTO getAllGenres() {
        HttpResponse<String> response = movieApiRequesting.sendRequest(MovieApiRequesting.GET_ALL_GENRES_URL);
        if (response.getStatus() == OK_STATUS) {
            return parsingService.parseGenres(response);
        } else {
            throw new ErrorStatusResponseException(response.getBody());
        }
    }

    private String buildMoviePageUrl(Integer pageNumber, String sort, List<String> genreIds) {
        StringBuilder url = new StringBuilder(MovieApiRequesting.SEARCH_MOVIES_URL + "?");
        if (pageNumber != null) {
            url.append("&page=").append(pageNumber);
        }
        if (sort != null) {
            url.append("&sort_by=").append(sort);
        }
        if (genreIds != null) {
            String genreIdsList = String.join(",", genreIds);
            url.append("&with_genres=").append(genreIdsList);
        }
        return url.toString();
    }

}
