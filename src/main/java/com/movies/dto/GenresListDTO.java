package com.movies.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class GenresListDTO {
    private List<GenreDTO> genres;
}
