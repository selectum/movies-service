package com.movies.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AverageVoteDTO {

    private Integer genreId;
    private String message;
    private Integer progress;
    private Double averageVote;

    public static AverageVoteDTO getStarted(Integer genreId) {
        return new AverageVoteDTO(genreId, "Counting average votes just started", 0, 0.);
    }

    public static AverageVoteDTO getProgress(Integer genreId, Integer progress, Double averageVote) {
        return new AverageVoteDTO(genreId, "Average vote is being calculated ", progress, averageVote);
    }

    public static AverageVoteDTO getResult(Integer genreId, Double averageVote) {
        return new AverageVoteDTO(genreId, "Average vote is calculated ", 100, averageVote);
    }

}
