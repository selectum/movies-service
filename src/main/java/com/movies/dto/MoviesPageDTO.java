package com.movies.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MoviesPageDTO {

    private Integer page;
    private Integer total_results;
    private Integer total_pages;
    private List<MovieDTO> results;

}
