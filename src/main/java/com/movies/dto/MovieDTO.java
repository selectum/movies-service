package com.movies.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MovieDTO {
    private Integer id;
    private Boolean adult;
    private List<GenreDTO> genres;
    private String original_language;
    private String original_title;
    private String overview;
    private Date release_date;
    private String title;
    private String status;
    private Double vote_average;
    private Integer vote_count;
}
