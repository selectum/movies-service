package com.movies.dto.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ParsingErrorDTO {

    public static final ParsingErrorDTO INSTANCE = new ParsingErrorDTO();

    private final String message = "Failed to parse response from the movies data store";
}
