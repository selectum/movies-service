package com.movies.dto.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MovieNotFoundDTO {
    private Integer id;
    private final String message = "Movie not found";
}
