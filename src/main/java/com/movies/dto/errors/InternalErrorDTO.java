package com.movies.dto.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class InternalErrorDTO {

    public static final InternalErrorDTO INSTANCE = new InternalErrorDTO();

    private final String message = "Unexpected error occurred";
}
