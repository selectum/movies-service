package com.movies.dto.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BadRequestDTO {

    public static final BadRequestDTO INSTANCE = new BadRequestDTO();

    private final String message = "Bad request";
}
