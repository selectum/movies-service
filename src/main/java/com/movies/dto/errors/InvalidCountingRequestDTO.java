package com.movies.dto.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class InvalidCountingRequestDTO {

    private final Integer genreId;
    private final String message = "The genre doesn't exist - heck the list of genres";

}
