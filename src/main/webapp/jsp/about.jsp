<!DOCTYPE HTML>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>

<head>
    <style>
        h3 {
            padding-left: 20px;
        }
    </style>
</head>

<br>

<h2>API</h2>

<h3><a href="<c:url value="/genres"/>">/genres</a> получить информацию о доступных жанрах</h3>

<h3>/movies: получить список фильмов</h3>
Возможные параметры: <br/>
- page: число от 1 до 1000 - номер страницы, например: <a href="<c:url value="/movies?page=10"/>">/movies?page=10</a> выдаёт десятую страницу <br/>
- genres: список id жанров, перечисленных через запятую, например: <a href="<c:url value="/movies?genres=35,27"/>">/movies?genres=35,27</a> включить в поиск только комендии и ужастики <br/>
- sort_by: сортировать результат поиска по одному из значений, например: <a href="<c:url value="/movies?sort_by=vote_average.desc"/>">/movies?sort_by=vote_average.desc</a> сортировка по рейтингу<br/>
Список значений:
<ul>
    <li>release_date.asc - сортировка по дате релиза в порядке возрастания</li>
    <li>release_date.desc - сортировка по дате релиза в порядке убывания</li>
    <li>original_title.asc - сортировка по названию в порядке возрастания</li>
    <li>original_title.desc - сортировка по названию в порядке убывания</li>
    <li>vote_average.asc - сортировка по рейтингу голосования в порядке возрастания</li>
    <li>vote_average.desc - сортировка по рейтингу голосования в порядке убывания</li>
</ul>


<h3>/movies/id: получить информацию о фильме по его id</h3>
Например: <br/>
<a href="<c:url value="/movies/777"/>">/movies/777</a> - выдаёт информацию о французском фильме "La Grande Illusion" <br/>
<a href="<c:url value="/movies/999"/>">/movies/999</a> - выдаёт 404, фильма с таким id нет

<h3>/votes/id: получить информацию о среднем рейтинге фильмов в жанре (id)</h3>
Например: <br/>
<a href="<c:url value="/votes/35"/>">/votes/35</a> - подсчитывает средний рейтинг для комедий <br/>

<h3>
    Формат возвращаемых данных
</h3>
<ul>
    В запросе нужно использовать следующие значения заголовка Accept
    <li>application/json (либо url.json) - ответ в формате json</li>
    <li>application/xml (либо url.xml) - ответ в формате xml (используется по умолчанию)</li>
</ul>



</body>
</html>
